<?php

namespace FreeElephants\RestAuthClient\Model;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface UserFactoryInterface
{
    public function createUser(array $data): UserInterface;
}