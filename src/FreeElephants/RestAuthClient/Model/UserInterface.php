<?php

namespace FreeElephants\RestAuthClient\Model;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface UserInterface
{
    public function getId();

    public function getLogin() : string;
}