<?php

namespace FreeElephants\RestAuthClient\Model;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class DefaultJsonMappingUserFactory implements UserFactoryInterface
{

    public function createUser(array $data): UserInterface
    {
        $id = $data['id'];
        $login = $data['login'];
        return new User($id, $login);
    }
}