<?php

namespace FreeElephants\RestAuthClient\Model;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class User implements UserInterface
{

    private $id;
    /**
     * @var string
     */
    private $login;

    public function __construct($id, string $login)
    {
        $this->id = $id;
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}