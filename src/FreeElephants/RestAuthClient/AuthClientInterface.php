<?php

namespace FreeElephants\RestAuthClient;

use FreeElephants\RestAuthClient\Model\UserInterface;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface AuthClientInterface
{

    public function isAuthKeyValid(string $authKey): bool;

    public function getUserByAuthKey(string $authKey): UserInterface;

    /**
     * @return array|UserInterface[]
     */
    public function getUsers(): array;

    public function getUserById($id): UserInterface;
}