<?php

namespace FreeElephants\RestAuthClient\HttpAdapter;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class GuzzleAdapter implements AdapterInterface
{

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get($path, array $queryParams = [], array $headers = []): ResponseInterface
    {
        $path .= $queryParams . http_build_query($queryParams);
        return $this->client->get($path, ['headers' => $headers]);
    }

    public function head($path, array $queryParams = [], array $headers = []): ResponseInterface
    {
        $path .= $queryParams . http_build_query($queryParams);
        return $this->client->head($path, ['headers' => $headers]);
    }
}