<?php

namespace FreeElephants\RestAuthClient\Exception;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class RuntimeException extends \RuntimeException implements AuthClientExceptionInterface
{

}