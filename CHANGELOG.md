# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.5.0
**Version breaking backward compatibility!**
### Added
- UserFactory abstration. 
### Changed 
- Vendor now is FreeElephants and project named as RestAuthClient. 
- `RestClient` accept `HttpClient` as first argument in constructor instead host configuration and `UserFactory` as second. 
### Removed
- `ServiceAuthClient`.
- `ClientInterface`. 

## 0.4.0
### Added
- `AuthClientInterface::getUserById()`. 

## 0.3.1
**Version breaking backward compatibility!**
### Renamed
- Fix type in name `AuthClientInterface`. 

## 0.3.0
**Version breaking backward compatibility!**

## Added:
- `ServiceInterface`
- `ServiceAuthClientInterface`

## Changed: 
- **`RestClient::__constructor()` require ServiceAuthClientInterface instance in first argument.**  

## Deprecated
- `ClientInterface` - use `AuthClientInterface` instead.
- `ClientInterface::getUser()` - use `::getUserByAuthKey()` instead. 

## Removed 
- **`User::isSystemUser()` method.**

## 0.2.0
Added
- Add User::isSystemUser() method. 

## 0.1.0 
### Fixed
- Use standard function `http_build_query` instead http_build_str from pecl.  

## 0.0.1 
### Added
- RestClient
- isAuthKeyValid method
- getUser method